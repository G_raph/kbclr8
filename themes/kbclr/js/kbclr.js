(function ($) {
  'use strict';
  Drupal.behaviors.kbclr = {
    setShareLinks: function() {
      // vars
      var $fblink = $('a.facebook-share-button');
      var $twlink = $('a.twitter-share-button');
      var $loc = window.location.href;
      var $tit = $('h1').html();
      $fblink.attr('data-href',$loc);
      $fblink.attr('data-title',$tit);
      $twlink.attr('data-href',$loc);
      $twlink.attr('data-title',$tit);
      // facebook
      $fblink.click(function (e) {
        e.preventDefault();
        console.log('geklikt op fb');
        var $url = $(this).attr('data-href');
        var title = $(this).attr('data-title');
        if (typeof FB != 'undefined') {
          FB.ui({
            method: 'share',
            href: $url,
          }, function (response) {
          });
        }
      });
      // twitter
      $twlink.click(function (e) {
        e.preventDefault();
        var loc = $(this).attr('data-href');
        var title = $(this).attr('data-title');
        window.open('http://twitter.com/intent/tweet?url=' + loc + '&text=' + title + '&', 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
      });
    },
    setHomepageVideo: function() {
      if ($('.path-frontpage').length) {
        $('.path-frontpage > .heading .homepage-video').hide();
        var video = $('.path-frontpage > .heading .homepage-video .field--name-field-video-url');
        var src = video.html();
        //console.log(src);
        if (src.length) {
          var $html = $('<video class="mainvideo" src="" autoplay muted loop></video>');
          if (!$('.mainvideo').length) {
            $html.prependTo($('.path-frontpage > .heading'));
          }
          var $videotag = $('.path-frontpage > .heading > .mainvideo');
          var isYoutube = src && src.match(/(?:youtu|youtube)(?:\.com|\.be)\/([\w\W]+)/i);
          if (isYoutube) {
            console.info('Youtube filmke');
            var id = isYoutube[1].match(/watch\?v=|[\w\W]+/gi);
            id = (id.length > 1) ? id.splice(1) : id;
            id = id.toString();
            var mp4url = "http://www.youtubeinmp4.com/redirect.php?video=";
            $videotag.attr('src',mp4url + id);
          } else {
            $videotag.attr('src',src);
          }
        }
      }
    },
    setParallax: function() {
      if ($('.page-node-type-article').length || $('.page-node-type-product').length) {
        var $mainImgUrl = $('article > .content > .field--type-image img').attr('src');
        console.info($mainImgUrl);
        var $imgbox = $('body > .imgbox');
        if (!$imgbox.length) {
          $('<div class="imgbox"></div>').insertAfter('.navbar-fixed-top');
        }
        $imgbox.parallax({
          imageSrc: $mainImgUrl,
          positionY: 'center'
        });
      }
    },
    setParagraphs: function() {
      var $banners = $('.paragraph--type--banner');
      if ($banners.length) {
        $banners.each(function(){
          var $banner = $(this);
          var bannerimgurl = $banner.find('.field--name-field-afbeelding img').attr('src');
          setTimeout(function(){
            $banner.parallax({
              imageSrc: bannerimgurl,
              positionY: 'center',
              zIndex: 1
            });
          },500);


        });
      }
    },
    setMasonry: function() {
      $('.path-artikels .view-articles').masonry({
        // options
        itemSelector: '.views-row',
        columnWidth: 300,
        gutter: 20
      });
    },
    setVelocity: function() {
      var $fadebox = $('.heading .fadebox');
      var $logoshield = $('.heading .shield');
      var $logoname = $('.heading .name');
      var $logosub = $('.heading .sub');
      var $question1 = $('.heading .question1');
      var $question2 = $('.heading .question2');
      $fadebox.velocity("fadeIn", { delay: 30000, duration: 2000 });
      $question1.velocity("fadeIn", { delay: 32000, duration: 2000 }).velocity("fadeOut", 2000);
      $question2.velocity("fadeIn", { delay: 36000, duration: 2000 }).velocity("fadeOut", 2000);
      $logoshield.velocity("fadeIn", { delay: 42000, duration: 2000 });
      $logoname.velocity("fadeIn", { delay: 42500, duration: 2000 });
      $logosub.velocity("fadeIn", { delay: 43000, duration: 2000 });
    },
    handleTeaserImgs: function() {
      $('.footer-top .view-articles .views-row').each(function(){
        var $this = $(this).find('.field--name-field-image');
        var imgurl = $this.find('img').attr('src');
        console.info(imgurl);
        $this.css('background','url('+imgurl+') no-repeat center / cover');
      });
    },
    load: function () {
      // custom js here
      this.setShareLinks();
      this.setHomepageVideo();
      this.setMasonry();
      this.setVelocity();
      this.handleTeaserImgs();
      this.setParallax();
      $(window).on('load resize',function(){
        Drupal.behaviors.kbclr.setParagraphs();
      });
    },
    attach: function (context) {

      this.setParallax();

    }

  };

  $(document).ready(function () {
    Drupal.behaviors.kbclr.load();
    //Drupal.behaviors.kbclr.setParallax();
  });

})(jQuery);
